from tkinter import *
from tkinter import ttk
from random import randint
import os.path

level = 1
turvis = 0
elusid = level * 10
kogemus = 0
relv = 1
raha = 10
vastaselvl = 0
v_elusid = 0
k_vigastus = 0
v_vigastus = 0
vastased = ['Troll','Vaim','Sookoll','Libahunt','Vampiir','Nõid','Draakon',\
            'Hiiglane','Peata Ratsanik']
vastaseid = len(vastased)
def kill(x):
    x.grid_remove()
def peida(x):
    x.grid_forget()
def küsi():
    global silt
    global nupp
    global nupp1
    global nupp2
    a.forget()
    if os.path.isfile(str(nimi.get())+'.txt') == True:
        peida(nimi)
        kill(silt)
        kill(nupp)
        root.title('Eelmised seiklused')
        silt = ttk.Label(a, text='Kas soovite eelmise Kangelasega edasi mängida? Keeldumise puhul kaob ta unustuste hõlma.')
        silt.grid(column=1, row=0, padx=5, pady=5, sticky=(N, W))
        nupp1 = ttk.Button(a, text="Jah", command=lae)
        nupp1.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
        nupp2 = ttk.Button(a, text="Ei", command=menu)
        nupp2.grid(column=1, row=3, padx=5, pady=5, sticky=(N, S, W, E))
    else:
        menu()
def lae():
    try:
        f = open(str(nimi.get())+'.txt','r')
        global level
        global elusid
        global turvis
        global kogemus
        global relv
        global raha
        level = int(f.readline().strip('\n'))
        elusid = int(f.readline().strip('\n'))
        turvis = int(f.readline().strip('\n'))
        kogemus = int(f.readline().strip('\n'))
        relv = int(f.readline().strip('\n'))
        raha = int(f.readline().strip('\n'))
    except:
        return menu()
    menu()
def salvesta():
    f = open(str(nimi.get())+'.txt','w')
    global level
    global elusid
    global turvis
    global kogemus
    global relv
    global raha
    f.write(str(level) + "\n")
    f.write(str(elusid) + "\n")
    f.write(str(turvis) + "\n")
    f.write(str(kogemus) + "\n")
    f.write(str(relv) + "\n")
    f.write(str(raha) + "\n")
def tagasi_s():
    kill(silt)
    kill(silt1)
    kill(silt2)
    kill(silt3)
    kill(nupp1)
    kill(nupp2)
    kill(nupp3)
    menu()
def tagasi_i():
    kill(silt)
    kill(silt1)
    kill(silt2)
    kill(silt3)
    kill(silt4)
    kill(silt5)
    kill(nupp1)
    menu()
def tagasi_v():
    kill(silt)
    kill(silt1)
    kill(silt2)
    kill(nupp1)
    kill(nupp2)
    kill(nupp3)
    return menu()
def kill_v():
    try:
        kill(silt)
        kill(silt1)
        kill(silt2)
    except:
        kill(silt)
def argpüks():
    global elusid
    global level
    global turvis
    kill_v()
    kill(nupp1)
    kill(nupp2)
    menu()
    elusid = level * 10
    sõnum = nimi.get() + ' on argpüks ja pistab elu eest jooksu!'
    messagebox.showinfo(message=sõnum)
võitle_d1 = [' virutab hoogsalt vastasele, kes kaotab ', ' lööb vastast, kes kaotab ',\
             ' vehib oma relvaga ja kogemata tabab vastast, kellel kaob ']
võitle_d2 = [' Vastane lööb vastu Kangelase turvist ning ei suuda seda läbida, Kangelane kaotab ',\
             ' Kangelane on samas tähelepanuta ning kaotab ', ' Vastasel õnnestub kiire vastulöök sooritada, Kangelane kaotab ']
def võitle():
    global silt
    global silt1
    global silt2
    global level
    global elusid
    global raha
    global kogemus
    global vastaselvl
    global v_elusid
    global k_vigastus
    global v_vigastus
    kill_v()
    if elusid > 0 and v_elusid > 0:
        k_löök = 2*level + randint(0,relv)
        v_löök = 2*vastaselvl + randint(1,vastaselvl)
        k_vigastus = randint(1, v_löök) - randint(0,turvis)
        v_vigastus = randint(1, k_löök)
        d1 = randint(0,(len(võitle_d1)-1))
        d2 = randint(1,(len(võitle_d2)-1))       
        if k_vigastus <= 0:
            k_vigastus = 0
            d2  = 0
        kill(silt)
        silt = ttk.Label(a, text=nimi.get()+ võitle_d1[d1] + str(v_vigastus) +\
                ' elu. ' + võitle_d2[d2] + str(k_vigastus) + ' elu.', wraplengt=400) 
        silt.grid(column=0, row=0, padx=5, pady=5, sticky=(N, W))
    elusid = elusid - k_vigastus
    if elusid < 0:
        elusid = 0
    v_elusid = v_elusid - v_vigastus
    if v_elusid < 0:
        v_elusid = 0
    silt1 = ttk.Label(a, text='Kangelasel järgi ' + str(elusid) + ' elu.') 
    silt1.grid(column=0, row=1, padx=5, pady=5, sticky=(N, W))
    silt2 = ttk.Label(a, text='Vaenlasel järgi ' + str(v_elusid) + ' elu.') 
    silt2.grid(column=0, row=2, padx=5, pady=5, sticky=(N, W))
    if elusid == 0 and v_elusid == 0:
        sõnum = 'Mõlemad on surmväsinud ja üritades vastast tabada koperdab ' + nimi.get() +\
                ' talle otsa. Mõlemad varisevad väsimusest kokku. Taas teadvusele tulles on ' + nimi.get() + ' üksi ning puutumatu.'
        messagebox.showinfo(message=sõnum)
        elusid = level * 10
        return tagasi_v()
    if elusid == 0:
        raha = raha - raha//10
        if raha < 0:
            raha = 0
        sõnum = nimi.get() + ' kaotab teadvuse ja ärkab taas turvalises kohas ja ... natuke vaesemalt!'
        messagebox.showinfo(message=sõnum)
        kogemus += vastaselvl
        if kogemus >= level**2*10:
            level += 1
            elusid = level*10
        elusid = level * 10
        tagasi_v()
    if v_elusid == 0:
        if vastaselvl > level:
            kasum = (vastaselvl+(vastaselvl-level))*randint(1,3)
        else:
            kasum = (vastaselvl+(level-vastaselvl))*randint(1,3)
        raha += kasum
        if raha < 0:
            raha = 0
        sõnum = 'Vastane kaotab võimsa löögi järel teadvuse, Kangelane kergendab ta kukrut ' + str(kasum) +\
                ' raha võrra ja läheb puhkab turvalises kohas!'
        messagebox.showinfo(message=sõnum)
        kogemus += vastaselvl*5
        if kogemus >= level**2*10:
            level += 1
            elusid = level*10
        elusid = level * 10
        tagasi_v()
def seiklema():
    global silt
    global nupp1
    global nupp2
    global nupp3
    global vastaselvl
    global v_elusid
    kill(nupp)
    kill(nupp1)
    kill(nupp2)
    kill(nupp3)
    vastane = vastased[randint(0,vastaseid-1)]
    vastaselvl = level + randint(-2,3)
    if relv > 5 or turvis > 5:
        vastaselvl += relv//5 + turvis //5
    if vastaselvl <= 0:
        vastaselvl = 1
    v_elusid = vastaselvl*10
    root.title('Otsides pahandusi')
    silt = ttk.Label(a, text="Järsku on sinu ees vaenulik " + vastane + " (level "+ str(vastaselvl) + ")" +", kes asub sind ründama!")
    silt.grid(column=0, row=0, padx=5, pady=5, sticky=(N, W))
    nupp1 = ttk.Button(a, text="Põgene", command=argpüks)
    nupp1.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
    nupp2 = ttk.Button(a, text="Võitle", command=võitle)
    nupp2.grid(column=2, row=2, padx=5, pady=5, sticky=(N, S, W, E))
    a.columnconfigure(1, weight=1)
    a.rowconfigure(1, weight=1)
def uuenda_r():
    global relv
    global raha
    global level
    if 0 > (raha - relv*15):
        sõnum = 'Sul pole piisavalt raha!'
        messagebox.showinfo(message=sõnum)
    else:
        raha = raha - relv*15
        relv += 1
        kill(silt1)
        kill(silt2)
        kill(silt3)
        sepp()
def uuenda_t():
    global raha
    global turvis
    global level
    if 0 > (raha - turvis*15):
        sõnum = 'Sul pole piisavalt raha!'
        messagebox.showinfo(message=sõnum)
    else:
        turvis += 1
        raha = raha - turvis*10
        kill(silt1)
        kill(silt2)
        kill(silt3)
        sepp()
def sepp():
    global silt
    global silt1
    global silt2
    global silt3
    global nupp1
    global nupp2
    global nupp3
    global raha
    global relv
    global turvis
    kill(silt)
    kill(nupp1)
    kill(nupp2)
    kill(nupp3)
    silt = ttk.Label(a, text="Sisened kohalikku sepakotta. Sepp on nõus sinu relva ja rüüd teatud summa eest paremaks tegema.")
    silt.grid(column=1, row=0, padx=5, pady=5, sticky=(N, W))
    silt1 = ttk.Label(a, text="Lase relva teritada: " + str(relv*15) + ' raha eest')
    silt1.grid(column=0, row=1, padx=5, pady=5, sticky=(N, W))
    silt2 = ttk.Label(a, text="Tugevda turvist: " + str((1+turvis)*10) + ' raha eest')
    silt2.grid(column=0, row=2, padx=5, pady=5, sticky=(N, W))
    silt3 = ttk.Label(a, text="Raha: " + str(raha))
    silt3.grid(column=0, row=3, padx=5, pady=5, sticky=(N, W))
    nupp1 = ttk.Button(a, text="Parem relv (+1 relv)", command=uuenda_r)
    nupp1.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
    nupp2 = ttk.Button(a, text="Parem turvis (+1 turvis)", command=uuenda_t)
    nupp2.grid(column=1, row=3, padx=5, pady=5, sticky=(N, S, W, E))
    nupp3 = ttk.Button(a, text="Lahku", command=tagasi_s)
    nupp3.grid(column=1, row=4, padx=5, pady=5, sticky=(N, S, W, E))
def ise():
    global silt
    global silt1
    global silt2
    global silt3
    global silt4
    global silt5
    global nupp1
    kill(silt)
    kill(nupp)
    kill(nupp2)
    kill(nupp3)
    silt = ttk.Label(a, text="Mis sa siin nüüd imetled " + nimi.get() + '?    ')
    silt.grid(column=1, row=0, padx=5, pady=5, sticky=(N, W))
    silt1 = ttk.Label(a, text="Level: " + str(level))
    silt1.grid(column=0, row=1, padx=5, pady=5, sticky=(N, W))
    silt2 = ttk.Label(a, text="Elusid: " + str(elusid))
    silt2.grid(column=0, row=2, padx=5, pady=5, sticky=(N, W))
    silt3 = ttk.Label(a, text="Turvis: " + str(turvis))
    silt3.grid(column=0, row=3, padx=5, pady=5, sticky=(N, W))
    silt4 = ttk.Label(a, text="Relv: " + str(relv))
    silt4.grid(column=0, row=4, padx=5, pady=5, sticky=(N, W))
    silt5 = ttk.Label(a, text="Raha: " + str(raha))
    silt5.grid(column=0, row=5, padx=5, pady=5, sticky=(N, W))
    nupp1 = ttk.Button(a, text="Tagasi maa peale", command=tagasi_i)
    nupp1.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
def lõpp():
    global elusid
    elusid = level * 10
    salvesta()
    root.destroy()
def menu():
    global silt
    global nupp1
    global nupp2
    global nupp3
    peida(nimi)
    kill(silt)
    kill(nupp)
    try:
        kill(nupp1)
        kill(nupp2)
    except:
        kill(silt)
    salvesta()
    root.title('Seiklused')
    silt = ttk.Label(a, text="Mida soovite teha " + nimi.get() + ' (level ' + str(level) + ') ?')
    silt.grid(column=1, row=0, padx=5, pady=5, sticky=(N, W))
    nupp1 = ttk.Button(a, text="Seiklema", command=seiklema)
    nupp1.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
    nupp2 = ttk.Button(a, text="Sepa juurde", command=sepp)
    nupp2.grid(column=1, row=3, padx=5, pady=5, sticky=(N, S, W, E))
    nupp3 = ttk.Button(a, text="Ennast imetlema", command=ise)
    nupp3.grid(column=1, row=4, padx=5, pady=5, sticky=(N, S, W, E))
    nupp4 = ttk.Button(a, text="Lõpeta seiklused", command=lõpp)
    nupp4.grid(column=1, row=5, padx=5, pady=5, sticky=(N, S, W, E))    
root = Tk()
root.title("Alusta")
a = Frame(root)
a.grid()
silt = ttk.Label(a, text="Teie Kangelase nimi")
silt.grid(column=0, row=0, padx=5, pady=5, sticky=(N, W))
nimi = ttk.Entry(a)
nimi.grid(column=1, row=0, padx=5, pady=5, sticky=(N, W, E))
nupp = ttk.Button(a, text="Alusta!", command=küsi)
nupp.grid(column=1, row=1, padx=5, pady=5, sticky=(N, S, W, E))
a.columnconfigure(0, weight=1)
a.rowconfigure(0, weight=1)
a.columnconfigure(1, weight=1)
a.rowconfigure(1, weight=1)
root.mainloop()
