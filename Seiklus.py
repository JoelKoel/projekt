from tkinter import *
from tkinter import ttk
from random import *
from math import *
import pickle
import os.path

level = 1
turvis = 1
elusid = level * 10
kogemus = 0
relv = 1
raha = 10
võlujooke = 3
sidemeid = 5
vastaselvl = 0
v_elusid = 0
k_vigastus = 0
v_vigastus = 0
x = 0
y = 0
kõrgeim = 1
kõrgeim_k = 'Hulgus'
vastased = ['Troll','Vaim','Sookoll','Libahunt','Vampiir','Nõid','Draakon',\
            'Hiiglane','Peata Ratsanik','Bandiit','Ninja','Piraat']
vastaseid = len(vastased)
kaart = [['kott','maja','kivihunnik','auk','sõnnikuhunnik'],
         ['vanker','tünn','seljakott','põõsas','koobas'],
         ['kirst','puuhunnik','osmik','liivahunnik','vaibarull'],
         ['puutüvi','kraav','haud','kasukas','lauahunnik'],
         ['mullahunnik','hütt','küngas','vaat','rahn']]
def kill(x):
    x.grid_remove()
def kinni():
    global a
    kill(a)
    a = Frame(root)
    a.grid()
def küsi():
    global a
    global nimi
    global kõrgeim
    global kõrgeim_k
    try:
        s = open('edetabel.txt', 'r')
        kõrgeim_k = str(s.readline().strip('\n'))
        kõrgeim = int(s.readline().strip('\n'))
    except:
        kõrgeim = 1
    nimi = nimi.get()
    kinni()
    if os.path.isfile(str(nimi)+'.txt') == True:
        root.title('Eelmised seiklused')
        silt = ttk.Label(a, text='Kas soovid eelmise Kangelasega edasi mängida? Keeldumise puhul kaob ta unustuste hõlma.',wraplengt=300)
        silt.grid(column=1, row=0, padx=5, pady=5, sticky=(N, W))
        nupp1 = ttk.Button(a, text="Jah", command=lambda: info('lae'))
        nupp1.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
        nupp2 = ttk.Button(a, text="Ei", command=menu)
        nupp2.grid(column=1, row=3, padx=5, pady=5, sticky=(N, S, W, E))
    else:
        menu()
def info(tegevus):
    global level
    global elusid
    global turvis
    global kogemus
    global relv
    global raha
    global võlujooke
    global sidemeid
    global x
    global y
    global kaart
    näitajad = [level, elusid, turvis, kogemus, relv, raha, võlujooke, sidemeid, x, y]
    if tegevus == 'lae':
        try:
            f = open(str(nimi)+'.txt','r')
            level = int(f.readline().strip('\n'))
            elusid = int(f.readline().strip('\n'))
            turvis = int(f.readline().strip('\n'))
            kogemus = int(f.readline().strip('\n'))
            relv = int(f.readline().strip('\n'))
            raha = int(f.readline().strip('\n'))
            võlujooke = int(f.readline().strip('\n'))
            sidemeid = int(f.readline().strip('\n'))
            x = int(f.readline().strip('\n'))
            y = int(f.readline().strip('\n'))
        except:
            return menu()
        menu()
    else:
        f = open(str(nimi)+'.txt','w')
        for i in näitajad:
            f.write(str(i) + "\n")
def uuenda_kaarti():
    global x
    global y
    global kaart
    for i in kaart:
        shuffle(i)
    shuffle(kaart)
    x = 0
    y = 0
    return vahe()
def ravi(vahend):
    global võlujooke
    global sidemeid
    global elusid
    if vahend == 'võlujook':
        if võlujooke > 0:
            võlujooke -= 1
            elusid = 10*level
            messagebox.showinfo(message=nimi + " vajab natuke julgustust ja tarbib vägijooki. " + nimi +\
                        " tunneb end taas täis elujõudu, et võitlust jätkata. " )
        else:
            messagebox.showinfo(message=nimi + " otsib põuest võlujooki, kuid oma ehmatuseks avastab, et \
        ühtegi pole enam järel ning peab hirmunult võitlust jätkama." )
        võitle()
    if vahend == 'sidemed':
        if sidemeid > 0:
            sidemeid -= 1
            if elusid == 10*level:
                messagebox.showinfo(message=nimi + " hakkab end suure innuga muumiaks mässima ning alles hiljem avastab, et tal polnud häda midagi. Sidemerull on nüüd must ja kasutu.")
                return vahe()
            elusid = elusid + 5*level
            if elusid > 10*level:
                elusid = 10*level
            messagebox.showinfo(message=nimi + " suudab suuremad haavad kinni siduda ning ta enesetunne paraneb." )
        else:
            messagebox.showinfo(message=nimi + " sooviks mõnda haava kinni siduda, kuid kahjuks ta ei leia ühtegi sidet." )
        vahe()
def argpüks():
    global elusid
    global level
    global turvis
    pääsemine = randint(1,5)
    if pääsemine != 1:
        if elusid > 2:
            elusid -= 1
        if elusid > level * 8:
            elusid = level * 8
        sõnum = nimi + ' on argpüks ja pistab elu eest jooksu! Vastasel õnnestub teda veel jooksu pealt tabada, kuid ' + nimi + ' on piisavalt kiire ning pääseb turvalisse kaugusesse.'
        messagebox.showinfo(message=sõnum)
        return vahe()
    else:
        messagebox.showinfo(message= nimi + ' üritab vaenlase eest ära joosta, kuid vaenlane jõuab talle järgi.')
        return võitle()
võitle_d1 = [' virutas hoogsalt vastasele', ' lõi vastast',\
             ' sai läraka linnusitta silma, mistõttu kaotas silmanägemise ja äsas pimesi',\
             ' leidis maast kivi ja virutas sellega vastasele',\
             ' viskas vastasele liiva näkku',\
             ' jooksis vastase hargivahelt läbi ja lõi teda munadesse',\
             ' hammustas vastast käest',\
             ' tegi vastasele „SU EMA“ nalja, vastane solvus ja sai sügavalt haavata',\
             ' astus vastasele valusasti varba peale',\
             ' solvas vastase rõivastust ja nimetas teda „odavaks“',\
             ' leidis maast kuusekäbi, mille ta vastasele tagumiku pihta viskas',\
             ' nägi, kuidas Vastane libises rohu peal ning sai kukkudes peapõrutuse',\
             ' vehib oma relvaga ja kogemata tabab vastast']
võitle_d2 = [' Vastane lööb vastu Kangelase turvist ning ei suuda seda läbida',\
             ' Kangelane on tähelepanematu ja koperdab oma jala taha', ' Vastasel õnnestub kiire vastulöök sooritada, Kangelane kaotab',\
             ' Vastane lükkas toore jõuga kangelase vastu kiviseina',\
             ' Kangelane üritas rakendada maadlusvõtet, kuid vastane nägi ta plaani läbi',\
             ' Vastane haaras kangelast kaelast, tõstes ta kõrgele üles',\
             ' Vastane hakkas koledasti laulma, mistõttu kangelane kaotas kõrvakuulmise',\
             ' Kangelane koperdas puujuure otsa, mistõttu ta lendas otse vastase jalge ette',\
             ' Vastane moondas end kangelase tüdruksõbraks, ajades kangelase hulluks',\
             ' Vastane hammustas kangelast kõrvast',\
             ' Vastane andis kangelasele terava kõrvakiilu',\
             ' Vastane kriimustas kangelast oma teravate küüntega',\
             ' Vastase diskotantsu liigutused ei olnud kangelasele ligilähedasedki, kaotas oma enesehinnangu']
def võitle(x=1):
    global silt
    global level
    global elusid
    global raha
    global võlujooke
    global kogemus
    global vastaselvl
    global v_elusid
    global k_vigastus
    global v_vigastus
    global kõrgeim
    kinni()
    if elusid > 0 and v_elusid > 0:
        õnn = 1
        ebaõnn = 1
        k_löök = 2*level + relv
        v_löök = 2*vastaselvl + randint(0,vastaselvl)
        if x != 1:
            loos = randint(1,2)
            if loos == 1:
                õnn = x
            else: ebaõnn = x
        k_vigastus = int(ebaõnn * (randint(1, v_löök) - randint(1,turvis)))
        v_vigastus = int(õnn * randint(1, k_löök))
        d1 = randint(0,(len(võitle_d1)-1))
        d2 = randint(1,(len(võitle_d2)-1))       
        if k_vigastus <= 0:
            k_vigastus = 0
            d2  = 0
        elusid = elusid - k_vigastus
        root.title('Võitlus')
        silt = ttk.Label(a, text=nimi+ võitle_d1[d1] + ', vastane kaotab ' +str(v_vigastus) +\
                ' elu. ' + võitle_d2[d2] + '. Kangelane kaotab ' + str(k_vigastus) + ' elu.', wraplengt=400) 
        silt.grid(column=0, row=0, padx=5, pady=5, sticky=(N, W))
        silt1 = ttk.Label(a, text="Mida soovite teha " + nimi + ' (level ' + str(level) + ') ?')
        silt1.grid(column=1, row=0, padx=5, pady=5, sticky=(N, S, W, E))
        nupp1 = ttk.Button(a, text="Proovi põgeneda", command=argpüks)
        nupp1.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
        nupp2 = ttk.Button(a, text="Võitle tavaliselt", command=lambda: võitle(1.5))
        nupp2.grid(column=1, row=1, padx=5, pady=5, sticky=(N, S, W, E))
        nupp3 = ttk.Button(a, text="Võitle julgemalt", command=lambda: võitle(2))
        nupp3.grid(column=2, row=1, padx=5, pady=5, sticky=(N, S, W, E))
        if elusid < 5:
            nupp4 = ttk.Button(a, text="Eluohtlik manööver", command=lambda: võitle(3))
            nupp4.grid(column=3, row=1, padx=5, pady=5, sticky=(N, S, W, E))
        silt2 = ttk.Label(a, text="Võlujooke: " + str(võlujooke))
        silt2.grid(column=0, row=3, padx=5, pady=5, sticky=(E))
        nupp = ttk.Button(a, text="Libista märjukest ja võitle edasi", command=lambda: ravi('võlujook'))
        nupp.grid(column=1, row=3, padx=5, pady=5, sticky=(N, S, W, E))
    if elusid < 0:
        elusid = 0
    v_elusid = v_elusid - v_vigastus
    if v_elusid < 0:
        v_elusid = 0
    silt1 = ttk.Label(a, text='Kangelasel järgi ' + str(elusid) + ' elu.') 
    silt1.grid(column=0, row=1, padx=5, pady=5, sticky=(N, W))
    silt2 = ttk.Label(a, text='Vaenlasel järgi ' + str(v_elusid) + ' elu.') 
    silt2.grid(column=0, row=2, padx=5, pady=5, sticky=(N, W))
    if elusid == 0 and v_elusid == 0:
        sõnum = 'Mõlemad on surmväsinud ja üritades vastast tabada koperdab ' + nimi +\
                ' talle otsa. Mõlemad varisevad väsimusest kokku. Taas teadvusele tulles on ' + nimi + ' üksi ning puutumatu, kuid raskelt haavatud.'
        messagebox.showinfo(message=sõnum)
        elusid = 1
        return vahe()
    if elusid == 0:
        if võlujooke == 0:
            messagebox.showerror(message= nimi + " saab vastaselt surmava löögi. Poolsurnult lamades otsib " + nimi +\
                                 " viimaseid tilgakesi võlujooki, kuid peab kurbusega tõdema, et on suutnud kõik pudelid tilgatuks juba juua. Nii lõppebki meie Kangelase seiklusrikas teekond... nägupidi mudas. ")
            os.remove(nimi+'.txt')
            messagebox.showinfo(message="Seni kuulsaim seikleja oli " + kõrgeim_k + ", level " + str(kõrgeim) + ", sinu teekond lõppes level " + str(level) + ".")
            if level > kõrgeim:
                s = open('edetabel.txt', 'w')
                s.write(str(nimi) + "\n")
                s.write(str(level) + "\n")
            return root.destroy()
        raha = raha - raha//10
        võlujooke -= 1
        if raha < 0:
            raha = 0
        sõnum = nimi + ' kaotab teadvuse... ärgates avastab ' + nimi + ' end taas linnast. Keegi heatahtlik mööduja oli Kangelase üles turgutanud ning turvaliselt linna toonud. Rahakukkur aga tunduks nagu varasemast kergem olevat.'
        messagebox.showinfo(message=sõnum)
        menu()
    if v_elusid == 0:
        if vastaselvl > level:
            kasum = (vastaselvl+(vastaselvl-level))*randint(1,9)
        else:
            kasum = (vastaselvl+(level-vastaselvl))*randint(1,9)
        raha += int(kasum*tase)
        if raha < 0:
            raha = 0
        sõnum = 'Vastane kaotab võimsa löögi järel teadvuse, Kangelane kergendab ta kukrut ' + str(kasum) +\
                ' raha võrra!'
        messagebox.showinfo(message=sõnum)
        kogemus += vastaselvl*5
        if kogemus >= level**2*10:
            level += 1
            elusid += 10
            messagebox.showinfo(message="Palju õnne, said uue leveli ja enesekindlust juurde!")
            if level == 10:
                messagebox.showinfo(message="Oled nüüd ringkonnas tuntud seikleja!")
            if level == 25:
                messagebox.showinfo(message="Oled nüüd kuulus seikleja!")
            if level == 50:
                messagebox.showinfo(message="Oled nüüd LEGENDAARNE seikleja!")
            if level == 100:
                messagebox.showinfo(message="Oled suure osa oma elust ringi rännanud ja sind teatakse kõikjal nagu pooljumalat, kelle kohta räägitakse palju erinevaid legende. Võid rahus vanaduspõlve nautida, sinu seiklused on igaveseks ajalukku jäädvustatud.")
                s = open('edetabel.txt', 'w')
                s.write(str(nimi) + "\n")
                s.write(str(level) + "\n")
            return uuenda_kaarti()
        vahe()
def vahe():
    global x
    global y
    kinni()
    root.title('Peale võtitlust')
    silt = ttk.Label(a, text=nimi +" tõmbab natuke hinge ja vaatab oma haavad üle." + ' (' + str(elusid) + ' elu järgi)')
    silt.grid(column=0, row=0, padx=5, pady=5, sticky=(N, W))
    silt1 = ttk.Label(a, text="Mida soovid teha " + nimi + ' (level ' + str(level) + ') ?')
    silt1.grid(column=1, row=0, padx=5, pady=5, sticky=(N, S, W, E))
    silt2 = ttk.Label(a, text= "Sidemeid: " + str(sidemeid))
    silt2.grid(column=0, row=2, padx=5, pady=5, sticky=(E))
    nupp1 = ttk.Button(a, text="Jätka teekonda", command=lambda: tee(y,x))
    nupp1.grid(column=1, row=1, padx=5, pady=5, sticky=(N, S, W, E))
    nupp2 = ttk.Button(a, text="Seo haavu", command=lambda: ravi('sidemed'))
    nupp2.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
    nupp3 = ttk.Button(a, text="Mine linna", command=menu)
    nupp3.grid(column=1, row=3, padx=5, pady=5, sticky=(N, S, W, E))
def vastane(vastane =''):
    global vastaselvl
    global v_elusid
    kinni()
    if vastane == '':
        vastane = vastased[randint(0,vastaseid-1)]
    tasakaal = 0
    if relv > 6 or turvis > 6:
        tasakaal += relv//6 + turvis //6
    vastaselvl = level + randint(-3,3+tasakaal)
    if vastaselvl <= 0:
        vastaselvl = 1
    v_elusid = vastaselvl*10
    root.title('Leides pahandusi')
    silt1 = ttk.Label(a, text="Mida soovid teha " + nimi + ' (level ' + str(level) + ') ?')
    silt1.grid(column=1, row=0, padx=5, pady=5, sticky=(N, S, W, E))
    silt = ttk.Label(a, text="Sinu ees on vaenulik " + vastane + " (level "+ str(vastaselvl) + ")" +", kes asub sind ründama!")
    silt.grid(column=0, row=0, padx=5, pady=5, sticky=(N, W))
    nupp1 = ttk.Button(a, text="Proovi põgeneda", command=argpüks)
    nupp1.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
    nupp2 = ttk.Button(a, text="Võitle", command=lambda: võitle())
    nupp2.grid(column=1, row=1, padx=5, pady=5, sticky=(N, S, W, E))
sündmus = ['Pärast pikka päeva tunned suurt janu ning otsustad otsida koha, kus veelähkrit täita. Pika otsimise peale leiad hüljatud kaevu piirkonnas, mis näeb neetud välja.',\
             'Hommikul ärgates on sul kõht nii tühi, et võiksid ära süüa kasvõi hobuse. Natuke aega ringi vaadates avastad tundmatu looma jäljed, mis suunduvad pimedasse paksu metsa.',\
             'Eelmisest võitlusest ei ole veel möödas palju aega, seetõttu tunned, et vajad veidi puhkust. Eemal asub mahajäetud maja, mis oleks ideaalne väikeseks tukastuskohaks, samas võib see endas peita ka ohtlikku vaenlast.',\
             'Oled oma teekonnaga jõudnud mägedesse. Kes oleks osanud arvata, et lumetorm nii kohutav võib olla! Kaks sammu edasi, kolm tagasi - sellise tempoga küll kaugele ei jõua. Õnneks on sinu ees kohe üks koopasarnane kohake.',\
             'Mida kaugemale liigud, seda soisemaks sinu jalgealune muutub. Oled kaotanud juba mõlemad oma saapad ning tahad puhkust oma jalgadele. Sinust vasakule jääb igivana puu, millese tundub olevat ehitatud maja. Tundub sobiv koht puhkuse tegemiseks.']
sündmused = [['Sinu kahtlused osutusid õigeks. Hetkega tekkis sinu ümber paks udukiht ning õhutemperatuur langes nullkraadidini. kaevust tõusis aegamööda kõhetu kogu, väike tütarlaps.\
Tema juuksed olid tumedad kui öö ning nahk hele kui lumi. Ei, see pole Lumivalgeke, see on Ringitüdruk.','Kaevus oli selge ja puhas allikavesi. Lähkrid täidetud, saad jätkata teekonda.',
               'Kaev oli kuivanud, pead janu kannatama.'],['Hommikusöögist võid suu puhtaks pühkida, sest ühel hetkel vaatad sa tõtt tigeda kentauriga, kelle ainus soov on kakelda ja verd valada.',
                'Jäljed viivad sind otse jaanalinnupesani. Hommikusöögiks munaomlett, pole paha!','Milline pettumus! Mets muutus nii tihedaks, et sealt ei saaks ükski loomgi läbi. Pead toitu mujalt leidma.'],
              ['Sinu kõhutunne ei eksinud. Olles vaevu väravast sisse saanud, on kärnas nõid valmis sind konnaks muutma ja oma nõiapatta viskama.',
               'Mõnikord võib ka vedada. Maja on küll maha jäetud, kuid eelmine omanik on sinna jätnud mitmeid vajalikke nõiajooke, mis kiirendaksid sinu paranemist.',
               'Jõudes lähemale, näed, et samahästi võiksid ka kivi peal puhata. Maja on ohtlikult lagunenud, ainult rumal läheks sinna sisse.'],
              ['Sinu õnnetuseks on koopas juba elanik olemas. Päris suur teine. Ja kohev. Ja valge. Ja sina veel arvasid, et Jetid on lihtsalt kuulujutt.',
               'See koobas on kui taeva kingitus. Täiuslik koht, kus ilma paranemist oodata.','Kah mul koobas. Halva nägemise tõttu pidasid järjekordset lumekuhja ekslikult pääseteeks.'],
              ['Sa oleks pidanud teadma, et soo on sama vaenulik nagu see on happeline. Mõirgega tormab uksest välja sookoll (Shrek!?), kes ei taha külalistest kuuldagi.',
               'Majani jõudes avastad, et selle omanik on kodust ära. Milline vedamine!','Lähemale jõudes saad aru, et sinu puumaja ei ole muud kui tavaline puu. Pead leidma muu koha puhkamiseks.']]
d1 = -1
def seik(s,d):
    global elusid
    global x
    global y
    kinni()
    root.title('Otsides seiklusi')
    silt1 = ttk.Label(a, text="Mida soovid teha " + nimi + ' (level ' + str(level) + ') ?')
    silt1.grid(column=1, row=0, padx=5, pady=5, sticky=(N, S, W, E))
    d2 = d
    if d == -1:   
        d = randint(0,(len(s)-1))
        silt = ttk.Label(a, text= sündmus[d], wraplengt=400)
        silt.grid(column=0, row=0, padx=5, pady=5, sticky=(N, W))
        nupp1 = ttk.Button(a, text="Uuri lähemalt", command=lambda: seik(sündmused,d))
        nupp1.grid(column=1, row=1, padx=5, pady=5, sticky=(N, S, W, E))
        nupp2 = ttk.Button(a, text="Jätka teekonda", command=lambda: tee(y,x) )
        nupp2.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
        nupp3 = ttk.Button(a, text="Mine linna", command=menu)
        nupp3.grid(column=1, row=3, padx=5, pady=5, sticky=(N, S, W, E))
    else:
        d = randint(0,(len(sündmused[0])-1))
        if d == 0:
            sõnum = sündmused[d2][d]
            vastas = ['Ringitüdruk','Kentaur','Kärnas Nõid','Jeti','Shrek']
            return vastane(vastas[d2]),messagebox.showinfo(message=sõnum)
        if d == 1:
            elusid = level * 10
            silt = ttk.Label(a, text= sündmused[d2][d] , wraplengt=400)
            silt.grid(column=0, row=0, padx=5, pady=5, sticky=(N, W))
            nupp1 = ttk.Button(a, text="Puhka oma elud täis ja jätka teekonda", command=lambda: tee(y,x))
            nupp1.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
        if d == 2:
            silt = ttk.Label(a, text= sündmused[d2][d], wraplengt=400)
            silt.grid(column=0, row=0, padx=5, pady=5, sticky=(N, W))
            nupp1 = ttk.Button(a, text="Jätka teekonda", command=lambda: tee(y,x))
            nupp1.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
            nupp3 = ttk.Button(a, text="Mine tagasi linna", command=menu)
            nupp3.grid(column=1, row=3, padx=5, pady=5, sticky=(N, S, W, E))
def loterii(asi):
    a = randint(1,4)
    if a <= 2:
        messagebox.showinfo(message="Sulle pakub huvi see " + asi + " tee ääres ning sa sead sammud selle poole. Järgmine hetk " + nimi + " kuuleb kedagi selja tagant lähenemas. ")
        return vastane()
    elif a == 3:
        messagebox.showinfo(message="Lähemalt uurides ei leia " + nimi + " midagi huvitavat ja jätakab oma teekonda.")
        return tee(y,x)
    else:
        global raha
        global võlujooke
        global sidemeid
        asjad = [' raha']
        h = int(randint(1,10)*tase)
        i = int(randint(1,5)*tase)
        j = int(level * randint(1,10)*tase)
        if j == 1:
            asjad[0] = ' raha'
        if h == 1:
            asjad.append('võlujoogi')
            võlujooke += 1
        if i == 1:
            asjad.append('sideme')
            sidemeid += i
        raha += j
        messagebox.showinfo(message= nimi + " leiab " + str(j) + (", ".join(str(lisa) for lisa in asjad)) + " ja jätkab rõõmsalt teekonda.")
        return tee(y,x)
def punkt(y,x,kordus=0):
    kinni()
    if randint(1,2) != 1:
        return vastane()
    d = randint(1,8)
    if d == 1:
        return seik(sündmus,-1)
    silt = ttk.Label(a, text= 'Kõndides mööda teed märkad, et tee ääres on kahtlane ' + kaart[y][x] + '.' , wraplengt=400)
    silt.grid(column=0, row=0, padx=5, pady=5, sticky=(N, W))
    silt1 = ttk.Label(a, text="Mida soovid teha " + nimi + ' (level ' + str(level) + ') ?')
    silt1.grid(column=1, row=0, padx=5, pady=5, sticky=(N, S, W, E))
    nupp1 = ttk.Button(a, text="Uuri lähemalt", command=lambda: loterii(str(kaart[y][x])))
    nupp1.grid(column=1, row=1, padx=5, pady=5, sticky=(N, S, W, E))
    nupp2 = ttk.Button(a, text="Jätka teekonda", command=lambda: tee(y,x))
    nupp2.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
def asukoht(kaugus,a,kõrgus,b):
    global x
    global y
    global tase
    x = x + a
    y = y + b
    tase += 0.1
    return punkt(y,x)
def tee(y=0,x=0,kordus=0):
    kinni()
    root.title('Suund')
    silt = ttk.Label(a, text=nimi + ' jõuab teede ristumiskohta ning tal tuleb teha valik, millist teed pidi edasi minna.')
    silt.grid(column=0, row=0, padx=5, pady=5, sticky=(N, W))
    if x-1 >= 0:
        nupp1 = ttk.Button(a, text="Läände", command=lambda: asukoht(x,-1,y,0))
        nupp1.grid(column=1, row=1, padx=5, pady=5)
    if y+1 < len(kaart):
        nupp2 = ttk.Button(a, text="Põhja", command=lambda: asukoht(x,0,y,1))
        nupp2.grid(column=2, row=0, padx=5, pady=5)
    if x+1 < len(kaart):
        nupp3 = ttk.Button(a, text="Itta", command=lambda: asukoht(x,1,y,0))
        nupp3.grid(column=3, row=1, padx=5, pady=5)
    if y-1 >= 0:
        nupp3 = ttk.Button(a, text="Lõunasse", command=lambda: asukoht(x,0,y,-1))
        nupp3.grid(column=2, row=2, padx=5, pady=5)
    nupp4 = ttk.Button(a, text="Mine linna", command=menu)
    nupp4.grid(column=2, row=1, padx=5, pady=5, sticky=(N, S, W, E))
def taasta(vigastus):
    global elusid
    global raha
    if elusid == level * 10:
        messagebox.showinfo(message="Pole sul häda midagi, aga aitäh raha eest.")
    if 0 > raha - ceil(vigastus/2):
        messagebox.showinfo(message='Sul pole piisavalt raha!')
    else:
        raha = raha - ceil(vigastus/2)
        elusid = elusid + vigastus
        ravitseja()
def lisa(vahend,hind):
    global raha
    global võlujooke
    global sidemeid
    global relv
    global turvis
    global level
    if vahend == 'võlujooke' or vahend == 'sidemeid':
        summa = level * hind
    if vahend == 'relv':
        summa = relv * hind
    if vahend =='turvis':
        summa = turvis * hind
    if 0 > (raha - summa):
        messagebox.showinfo(message='Sul pole piisavalt raha!')
        if vahend == 'võlujooke' or vahend == 'sidemeid':
            return ravitseja()
        if vahend == 'relv' or vahend == 'turvis':
            return sepp()
    else:
        raha = raha - summa
        if hind == 15:
            relv += 1
            sepp()
        if hind == 10:
            turvis += 1
            sepp()
        if hind == 5:
            võlujooke += 1
            ravitseja()
        if hind == 3:
            sidemeid += 1
            ravitseja()
def ravitseja():
    global raha
    global võlujooke
    global sidemeid
    global elusid
    kinni()
    vigastus = (level*10)-elusid
    if raha < (level*10)-elusid:
        if (level*10-elusid) > 2*raha:
            vigastus = 2*raha
    silt = ttk.Label(a, text="Sisened kahtlasesse uberikku, kust leiad ühe vanema naisterahva. Ravitseja on \
nõus sinu haavu ravitsema ja sinuga kauplema teatud summa eest.",wraplengt=400)
    silt.grid(column=1, row=0, padx=5, pady=5, sticky=(N, W))
    silt1 = ttk.Label(a, text="Lase kõik haavad ravida: " + str(ceil(vigastus/2)) + ' raha eest')
    silt1.grid(column=0, row=1, padx=5, pady=5, sticky=(N, W))
    silt2 = ttk.Label(a, text="Osta võlujook: " + str(level*5) + ' raha eest')
    silt2.grid(column=0, row=2, padx=5, pady=5, sticky=(N, W))
    silt3 = ttk.Label(a, text="Osta sidemeid: " + str(level*3) + ' raha eest')
    silt3.grid(column=0, row=3, padx=5, pady=5, sticky=(N, W))
    silt4 = ttk.Label(a, text="Elusid: " + str(elusid))
    silt4.grid(column=0, row=4, padx=5, pady=5, sticky=(N, W))
    silt5 = ttk.Label(a, text="Võlujooke: " + str(võlujooke))
    silt5.grid(column=0, row=5, padx=5, pady=5, sticky=(N, W))
    silt6 = ttk.Label(a, text="Sidemeid: " + str(sidemeid))
    silt6.grid(column=0, row=6, padx=5, pady=5, sticky=(N, W))
    silt7 = ttk.Label(a, text="Raha: " + str(raha))
    silt7.grid(column=0, row=7, padx=5, pady=5, sticky=(N, W))
    nupp1 = ttk.Button(a, text="Taasta "+ str(vigastus) +" elu ", command=lambda: taasta(vigastus))
    nupp1.grid(column=1, row=1, padx=5, pady=5, sticky=(N, S, W, E))
    nupp2 = ttk.Button(a, text="Osta märjukest", command=lambda: lisa('võlujooke',5))
    nupp2.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
    nupp3 = ttk.Button(a, text="Osta sidemeid", command=lambda: lisa('sidemeid',3))
    nupp3.grid(column=1, row=3, padx=5, pady=5, sticky=(N, S, W, E))
    nupp4 = ttk.Button(a, text="Lahku", command=menu)
    nupp4.grid(column=1, row=7, padx=5, pady=5, sticky=(N, S, W, E))
def sepp():
    global raha
    global relv
    global turvis
    kinni()
    silt = ttk.Label(a, text="Sisened kohalikku sepakotta. Sepp on nõus sinu relva ja rüüd teatud summa eest paremaks tegema.",wraplengt=300)
    silt.grid(column=1, row=0, padx=5, pady=5, sticky=(N, W))
    silt1 = ttk.Label(a, text="Lase relva teritada: " + str(relv*15) + ' raha eest')
    silt1.grid(column=0, row=1, padx=5, pady=5, sticky=(N, W))
    silt2 = ttk.Label(a, text="Tugevda turvist: " + str(turvis*10) + ' raha eest')
    silt2.grid(column=0, row=2, padx=5, pady=5, sticky=(N, W))
    silt3 = ttk.Label(a, text="Raha: " + str(raha))
    silt3.grid(column=0, row=3, padx=5, pady=5, sticky=(N, W))
    nupp1 = ttk.Button(a, text="Parem relv (+1 relv)", command=lambda: lisa('relv',15))
    nupp1.grid(column=1, row=1, padx=5, pady=5, sticky=(N, S, W, E))
    nupp2 = ttk.Button(a, text="Parem turvis (+1 turvis)", command=lambda: lisa('turvis',10))
    nupp2.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
    nupp3 = ttk.Button(a, text="Lahku", command=menu)
    nupp3.grid(column=1, row=3, padx=5, pady=5, sticky=(N, S, W, E))
def ise():
    kinni()
    silt = ttk.Label(a, text="Mis sa siin nüüd imetled, " + nimi + '?    ')
    silt.grid(column=1, row=0, padx=5, pady=5, sticky=(N, S, W, E))
    silt1 = ttk.Label(a, text="Level: " + str(level))
    silt1.grid(column=0, row=1, padx=5, pady=5, sticky=(N, W))
    silt2 = ttk.Label(a, text="Elusid: " + str(elusid))
    silt2.grid(column=0, row=2, padx=5, pady=5, sticky=(N, W))
    silt3 = ttk.Label(a, text="Turvis: " + str(turvis))
    silt3.grid(column=0, row=3, padx=5, pady=5, sticky=(N, W))
    silt4 = ttk.Label(a, text="Relv: " + str(relv))
    silt4.grid(column=0, row=4, padx=5, pady=5, sticky=(N, W))
    silt5 = ttk.Label(a, text="Raha: " + str(raha))
    silt5.grid(column=0, row=5, padx=5, pady=5, sticky=(N, W))
    silt6 = ttk.Label(a, text="Võlujooke: " + str(võlujooke))
    silt6.grid(column=0, row=6, padx=5, pady=5, sticky=(N, W))
    silt7 = ttk.Label(a, text="Sidemeid: " + str(sidemeid))
    silt7.grid(column=0, row=7, padx=5, pady=5, sticky=(N, W))
    nupp1 = ttk.Button(a, text="Tagasi maa peale", command=menu)
    nupp1.grid(column=1, row=7, padx=5, pady=5, sticky=(N, S, W, E))
def lõpp():
    global elusid
    elusid = level * 10
    info('salvesta')
    root.destroy()
def menu():
    global tase
    kinni()
    tase = 1
    info('salvesta')
    root.title('Linnas')
    silt = ttk.Label(a, text="Mida soovid teha " + nimi + ' (level ' + str(level) + ') ?')
    silt.grid(column=1, row=0, padx=5, pady=5, sticky=(N, S, W, E))
    nupp1 = ttk.Button(a, text="Seiklema", command=lambda: tee(y,x))
    nupp1.grid(column=1, row=2, padx=5, pady=5, sticky=(N, S, W, E))
    nupp2 = ttk.Button(a, text="Sepa juurde", command=sepp)
    nupp2.grid(column=1, row=3, padx=5, pady=5, sticky=(N, S, W, E))
    nupp3 = ttk.Button(a, text="Ravitseja juurde", command=ravitseja)
    nupp3.grid(column=1, row=4, padx=5, pady=5, sticky=(N, S, W, E))
    nupp4 = ttk.Button(a, text="Ennast imetlema", command=ise)
    nupp4.grid(column=1, row=5, padx=5, pady=5, sticky=(N, S, W, E))
    nupp5 = ttk.Button(a, text="Lõpeta seiklused", command=lõpp)
    nupp5.grid(column=1, row=6, padx=5, pady=5, sticky=(N, S, W, E))
               
root = Tk()
root.title("Alusta")
root.resizable(0,0)
a = Frame(root)
a.grid()
silt = ttk.Label(a, text="Teie Kangelase nimi")
silt.grid(column=0, row=0, padx=5, pady=5, sticky=(N, W))
nimi = ttk.Entry(a)
nimi.grid(column=1, row=0, padx=5, pady=5, sticky=(N, W, E))
nupp = ttk.Button(a, text="Alusta!", command=küsi)
nupp.grid(column=1, row=1, padx=5, pady=5, sticky=(N, S, W, E))
root.mainloop()
